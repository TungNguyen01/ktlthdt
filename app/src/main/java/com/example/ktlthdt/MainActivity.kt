package com.example.ktlthdt

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.example.ktlthdt.ui.auth.signin.SignInFragment


class MainActivity : AppCompatActivity() {
    private lateinit var bt : Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//        bt = findViewById(R.id.bt)
//        bt.setOnClickListener{
//            navigateToMainScreen()
//        }
        val signInFragment = SignInFragment()
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, signInFragment)
            .commit()
    }
    fun navigateToMainScreen() {
        val mainMenuFragment = SignInFragment()

        val fragmentManager = supportFragmentManager
        val transaction = fragmentManager.beginTransaction()

        transaction.replace(R.id.container, mainMenuFragment)
        transaction.addToBackStack(null)

        transaction.commit()
    }

}