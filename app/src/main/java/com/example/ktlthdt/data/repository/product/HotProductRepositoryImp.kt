package com.example.ktlthdt.data.repository.product

import com.example.ktlthdt.data.model.HotBookList
import com.example.ktlthdt.data.model.ProductInfoList
import com.example.ktlthdt.data.repository.hotbook.HotBookRepository
import com.example.ktlthdt.data.repository.hotbook.HotBookRepositoryImp
import com.example.ktlthdt.datasource.IDataSource
import retrofit2.Response

class HotProductRepositoryImp(private val dataSource: IDataSource): HotProductRepository {
    override suspend fun getProductInfo(id: Int): Response<ProductInfoList>? {
        return dataSource.getProductInfo(id)
    }
}