package com.example.ktlthdt.data.repository.banner

import com.example.ktlthdt.data.model.BannerList
import com.example.ktlthdt.datasource.IDataSource
import retrofit2.Response

class BannerRepositoryImp(private val iDataSource: IDataSource) : BannerRepository {
    override suspend fun getBanner(): Response<BannerList>? {
        return iDataSource.getBanner()
    }
}