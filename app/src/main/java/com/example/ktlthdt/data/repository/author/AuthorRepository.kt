package com.example.ktlthdt.data.repository.author

import com.example.ktlthdt.data.model.Author
import com.example.ktlthdt.data.model.AuthorList
import com.example.ktlthdt.data.model.AuthorResult
import retrofit2.Response

interface AuthorRepository {


    suspend fun getAllAuthors() : Response<AuthorList>?
    suspend fun getAuthor(authorId: Int): Response<AuthorResult>?
}