package com.example.ktlthdt.data.repository.banner

import com.example.ktlthdt.data.model.BannerList
import retrofit2.Response

interface BannerRepository {
    suspend fun getBanner() : Response<BannerList>?
}