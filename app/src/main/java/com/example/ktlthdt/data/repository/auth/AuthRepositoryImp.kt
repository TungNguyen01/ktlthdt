package com.example.ktlthdt.data.repository.auth
import com.example.ktlthdt.data.api.RetrofitClient
import com.example.ktlthdt.data.model.LoginResponse
import retrofit2.Response

class AuthRepositoryImp : AuthRepository {
    override suspend fun login(email: String, password: String): Response<LoginResponse> {
        return RetrofitClient.apiService.login(email, password)
    }
}