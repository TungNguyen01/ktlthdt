package com.example.ktlthdt.data.repository.auth

import com.example.ktlthdt.data.model.LoginResponse
import retrofit2.Response

interface AuthRepository {
    suspend fun login(email: String, password: String): Response<LoginResponse>
}