package com.example.ktlthdt.data.model

data class HistorySearch(
    val historyLocal: String?,
    val historySuggest: Product?,
)