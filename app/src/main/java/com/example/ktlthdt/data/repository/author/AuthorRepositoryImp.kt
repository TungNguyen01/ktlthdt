package com.example.ktlthdt.data.repository.author

import com.example.ktlthdt.data.model.Author
import com.example.ktlthdt.data.model.AuthorList
import com.example.ktlthdt.data.model.AuthorResult
import com.example.ktlthdt.datasource.IDataSource
import retrofit2.Response

class AuthorRepositoryImp(private val iDataSource: IDataSource) : AuthorRepository {

    override suspend fun getAllAuthors(): Response<AuthorList>? {
        return iDataSource.getAllAuthor()
    }
    override suspend fun getAuthor(authorId: Int): Response<AuthorResult>? {
        return iDataSource.getAuthor(authorId)
    }
}