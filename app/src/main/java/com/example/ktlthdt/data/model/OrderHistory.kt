package com.example.ktlthdt.data.model

data class OrderHistory(
    val header: String?,
    val order: Order?,
)