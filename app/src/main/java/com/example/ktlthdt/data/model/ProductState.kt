package com.example.ktlthdt.data.model

data class ProductState(
    val products: List<Product>?,
    val isDefaultState: Boolean
)