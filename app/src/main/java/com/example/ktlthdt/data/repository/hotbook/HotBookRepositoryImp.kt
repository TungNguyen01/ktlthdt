package com.example.ktlthdt.data.repository.hotbook

import com.example.ktlthdt.data.model.HotBookList
import com.example.ktlthdt.datasource.IDataSource
import retrofit2.Response

class HotBookRepositoryImp(private val dataSource: IDataSource) : HotBookRepository {
    override suspend fun getHotBook(): Response<HotBookList>? {
        return dataSource.getBooks()
    }
}