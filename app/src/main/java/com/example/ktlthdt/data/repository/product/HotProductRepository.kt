package com.example.ktlthdt.data.repository.product

import com.example.ktlthdt.data.model.ProductInfoList
import retrofit2.Response

interface HotProductRepository {
    suspend fun getProductInfo(id: Int): Response<ProductInfoList>?
}