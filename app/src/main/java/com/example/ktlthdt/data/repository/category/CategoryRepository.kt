package com.example.ktlthdt.data.repository.category

import com.example.ktlthdt.data.model.CategoryList
import com.example.ktlthdt.data.model.HotBookList
import retrofit2.Response

interface CategoryRepository {
    suspend fun getCategory(): Response<CategoryList>?
}