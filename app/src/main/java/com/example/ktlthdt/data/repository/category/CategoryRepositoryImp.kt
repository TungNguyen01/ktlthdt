package com.example.ktlthdt.data.repository.category

import com.example.ktlthdt.data.model.CategoryList
import com.example.ktlthdt.datasource.IDataSource
import retrofit2.Response

class CategoryRepositoryImp(private val dataSource: IDataSource) : CategoryRepository{
    override suspend fun getCategory(): Response<CategoryList>? {
        return dataSource.getCategory()
    }
}