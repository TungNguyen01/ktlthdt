package com.example.ktlthdt.data.model

data class Book(
    val img : String,
    val name: String,
    val price: String,
    val sprice: String
    )
