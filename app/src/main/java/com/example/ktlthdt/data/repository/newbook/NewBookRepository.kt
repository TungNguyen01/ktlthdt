package com.example.ktlthdt.data.repository.newbook

import com.example.ktlthdt.data.model.NewArrivalList
import retrofit2.Response

interface NewBookRepository {
    suspend fun getNewBook(): Response<NewArrivalList>?
}