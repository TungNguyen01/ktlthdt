package com.example.ktlthdt.data.repository.hotbook

import com.example.ktlthdt.data.model.HotBookList
import retrofit2.Response

interface HotBookRepository {
    suspend fun getHotBook(): Response<HotBookList>?
}