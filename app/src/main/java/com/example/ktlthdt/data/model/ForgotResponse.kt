package com.example.ktlthdt.data.model

data class ForgotResponse(
    val message : String
)
