package com.example.ktlthdt.data.model

import com.google.gson.annotations.SerializedName

data class AuthorResult(
    @SerializedName("result" ) var author : Author,
)