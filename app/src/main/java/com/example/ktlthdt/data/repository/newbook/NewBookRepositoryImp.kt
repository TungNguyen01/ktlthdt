package com.example.ktlthdt.data.repository.newbook

import com.example.ktlthdt.data.model.NewArrivalList
import com.example.ktlthdt.datasource.IDataSource
import retrofit2.Response

class NewBookRepositoryImp(private val dataSource: IDataSource) : NewBookRepository {
    override suspend fun getNewBook(): Response<NewArrivalList>? {
        return dataSource.getNewBook()
    }
}