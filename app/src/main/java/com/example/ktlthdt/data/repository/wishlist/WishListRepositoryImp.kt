package com.example.ktlthdt.data.repository.wishlist

import com.example.ktlthdt.data.model.Messeage
import com.example.ktlthdt.data.model.WishlistList
import com.example.ktlthdt.datasource.IDataSource
import retrofit2.Response

class WishListRepositoryImp(private val dataSource: IDataSource) : WishListRepository {
    override suspend fun addItemToWishList(productId: Int): Response<Messeage>? {
        return dataSource.addItemToWishList(productId)
    }

    override suspend fun removeItemInWishList(productId: Int): Response<Messeage>? {
        return dataSource.removeItemInWishList(productId)
    }

    override suspend fun getWishlist(): Response<WishlistList>? {
        return dataSource.getWishlist()
    }


}