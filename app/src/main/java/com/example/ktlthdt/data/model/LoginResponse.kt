package com.example.ktlthdt.data.model

data class LoginResponse(
    val accessToken: String,
    val customer: Customer,
)