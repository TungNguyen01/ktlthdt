package com.example.ktlthdt.data.model

data class CartResponseList(
    val cart : List<CartResponse>
)
