package com.example.ktlthdt.ui.adapter

interface OnItemClickListener {
    fun onItemClick(position: Int)
}