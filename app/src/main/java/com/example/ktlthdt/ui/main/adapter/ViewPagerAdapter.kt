package com.example.ktlthdt.ui.main.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager2.adapter.FragmentStateAdapter


//class ViewPagerAdapter(fm: FragmentManager, behavior: Int) : FragmentStatePagerAdapter(fm, behavior) {
//
//    override fun getItem(position: Int): Fragment {
//        return when (position) {
//            0 -> HomeFragment()
//            1 -> SearchFragment()
//            2 -> WishlistFragment()
//            3 -> ShoppingbagFragment()
//            else -> throw IllegalArgumentException("Invalid position: $position")
//        }
//    }
//
//    override fun getCount(): Int {
//        return 4
//    }
//}
class ViewPagerAdapter(activity: FragmentActivity, private val fragments: List<Fragment>) :
    FragmentStateAdapter(activity) {

    override fun getItemCount(): Int = fragments.size

    override fun createFragment(position: Int): Fragment = fragments[position]
}
