package com.example.ktlthdt.ui.main.wishlist

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.ktlthdt.R

import com.example.ktlthdt.databinding.FragmentWishlistBinding
import com.example.ktlthdt.ui.adapter.OnItemClickListener
import com.example.ktlthdt.ui.main.adapter.WishListAdapter
import com.example.ktlthdt.ui.main.wishlist.viewmodel.WishlistViewModel
import com.example.ktlthdt.ui.profile.ProfileFragment
import com.example.ktlthdt.utils.FormatMoney

class WishlistFragment : Fragment() {
    private lateinit var viewModel: WishlistViewModel
    private lateinit var wishListAdapter: WishListAdapter
    private var total = 0.0
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(WishlistViewModel::class.java)
        val binding = FragmentWishlistBinding.inflate(inflater, container, false)
        val formatMoney = FormatMoney()
        viewModel.getWishlist()
        wishListAdapter = WishListAdapter()
        viewModel.wishlist.observe(viewLifecycleOwner, {wishlist ->
            wishListAdapter.updateData(wishlist)
        })
        binding?.apply {
            imageProfile.setOnClickListener {
                val profileFragment = ProfileFragment()
                parentFragmentManager.beginTransaction()
                    .replace(R.id.container, profileFragment)
                    .addToBackStack("WishlistFragment")
                    .commit()
            }
            textCheckout.setOnClickListener {
                viewModel.addAllWishList()
                Toast.makeText(context, "Thêm thành công", Toast.LENGTH_SHORT).show()
            }
            swipeRefresh.setOnRefreshListener {
                swipeRefresh.isRefreshing=false
                viewModel.getWishlist()
            }
            viewModel.wishlist.observe(viewLifecycleOwner) { cart ->
                wishListAdapter.updateData(cart)
                total = 0.0
                for (i in cart) {
                    total += i.price.toDouble()
                }
                binding.textPrice.text = total.let { formatMoney.formatMoney(it.toLong()) }.toString()
            }
        }
        binding.recyclerviewWishlist.apply {
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            adapter = wishListAdapter
            var total = 0.00
            addItemToBag()
            val swipeCallback = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
                override fun onMove(
                    recyclerView: RecyclerView,
                    viewHolder: RecyclerView.ViewHolder,
                    target: RecyclerView.ViewHolder
                ): Boolean {
                    return false
                }
                override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                    val position = viewHolder.adapterPosition
                    val deletedItem = viewModel.wishlist.value?.get(position)
                    deletedItem?.let {
                        viewModel.removeItemInWishList(it.product_id)
                        viewModel.getWishlist()
                        wishListAdapter.notifyDataSetChanged()
                    }
                }
            }
            val itemTouchHelper = ItemTouchHelper(swipeCallback)
            itemTouchHelper.attachToRecyclerView(this)
        }
        return binding.root
    }

    private fun addItemToBag(){
        wishListAdapter.setOnItemClickListener(object : OnItemClickListener {
            override fun onItemClick(position: Int) {
                val additem = viewModel.wishlist.value?.get(position)
                Log.d("sontung", additem.toString())
                additem?.let {
                    viewModel.addItemToCart(it.product_id)
                    wishListAdapter.notifyDataSetChanged()
                }
                Toast.makeText(context, "OKOKOK", Toast.LENGTH_SHORT).show()
            }
        })
    }
    override fun onResume() {
        super.onResume()
        viewModel.getWishlist()
    }

}